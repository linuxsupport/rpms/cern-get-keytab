# Auxiliary tools

* `inspect_logs.sh`: This is a simple tool to inspect log files from the image-ci pipeline

Example output:

```
$ ./inspect_logs.sh pythonport
Downloading artifacts from the latest pipeline of linuxsupport/rpms/cern-get-keytab/pythonport (4409382)
Artifact download directory is: /tmp/tmp.zhi2m0j7en
Downloading artifacts for job: 24157325
Downloading artifacts for job: 24157324
Downloading artifacts for job: 24157323
Unzipping artifacts
Displaying how many times a password reset was called
      6 /tmp/tmp.zhi2m0j7en/citest-4409407-ezcot-messages.log
      6 /tmp/tmp.zhi2m0j7en/citest-4409407-gtzow-messages.log
      6 /tmp/tmp.zhi2m0j7en/citest-4409407-j3lcx-messages.log
Displaying how many times 'Access Denied' was returned from msktuil
$
```
