% CERN-GET-KEYTAB(1)
% Jaroslaw Polok <jaroslaw.polok@cern.ch>, Ben Morrice <ben.morrice@cern.ch>
% October 2023

# NAME

cern-get-keytab - utility to acquire and store CERN host/service or user Kerberos keytab.

# DESCRIPTION

cern-get-keytab retrieves from CERN Active Directory Kerberos host/service identities and
stores these in a local keytab file. It can also generate user keytabs.

# SYNOPSIS

>         cern-get-keytab   [--help]
>
>         cern-get-keytab   [--keytab FILENAME][--force][--verbose][--debug]
>                           [--passwordsmb][--leavekrb5cfg]
>
>         cern-get-keytab   [--keytab FILENAME][--service SRVC][--force]
>                           [--isolate][--hostname HOSTNAME]
>                           [--enctypes ENCTYPE|ENCTYPE|.. ]
>                           [--passwordsmb][--leavekrb5cfg][--verbose][--debug]
>
>         cern-get-keytab  --keytab FILENAME --alias ALIAS
>                           [--service SRVC][--force]
>                           [--leavekrb5cfg][--verbose][--debug]
>
>         cern-get-keytab  --keytab FILENAME --cname ALIAS
>                           [--service SRVC][--force]
>                           [--leavekrb5cfg][--verbose][--debug]
>
>         cern-get-keytab   [--keytab FILENAME][--force][--verbose][--debug]
>                           [--isolate][--alias ALIAS][--service SERVICE]
>
>         cern-get-keytab   [--keytab FILENAME][--force][--verbose][--debug]
>                           [--isolate][--cname ALIAS][--service SERVICE]
>
>         cern-get-keytab --user --keytab FILENAME
>                           [--login LOGIN] [--password PASSWORD ]
>                           [--leavekrb5cfg][--verbose][--debug]

# OPTIONS

**\-\-help**

    Shows this help description

**\-\-force**

    Appends new to already existing valid host/service keytab file, invalidating current entries.

**\-\-service SRVC**

    Acquires keytab for given service rather than host. SRVC name is case-sensitive
    (example common service names: HTTP, cvs, ...). See also --isolate option.
    If not specified the default service: host is used.

**\-\-remove SRVC**

    Removes keys for named service from Active Directory.

**\-\-keytab FILENAME**

    Stores retrieved keytab in FILENAME rather than default /etc/krb5.keytab

**\-\-isolate**

    Active Directory provides keytabs for host and services using same encryption keys.
    This option allows for creating service keytab with encryption keys different that host keytab
    encryption keys for improved security. By default such keytabs are stored as /etc/krb5.keytab.SRVC
    unless --keytab option is specified.

**\-\-hostname HOSTNAME**

    Use HOSTNAME for requesting Active Directory password reset.
    This option is to be used ONLY when system has multiple interfaces / ip addresses
    allocated and keytab is required for hostname corresponding to a non-default route interface.

    System network interface corresponding to HOSTNAME must be defined and active before
    running cern-get-keytab with this option specified.

    This option will not function while using DNS host aliases.
    See next option.

**\-\-alias ALIAS**
**\-\-cname ALIAS**

    Acquire keytab for CERN DNS Dynamic Alias (delegated zone/subdomain),
    or Landb load balancing alias (\*--load-X-)
    This operation will only succeed if executed on a system
    being active member of DNS alias.

**\-\-user**

    Generate keytab for user account, this option requires specifying
    user account password and optionally user login.

    Such keytab can be used for authentication with kinit:

    # kinit -kt /path/to/user.keytab.file login

    WARNING: store user keytab in protected location: this file
    contains your user credentials which can be used to authenticate
    until account password is changed.

**\-\-password PASSWORD**

    User account password for user keytab generation.
    If not provided, will be asked interactively.
    Special characters in password need to be shell-escaped.

**\-\-login LOGIN**

    User account login for user keytab generation.
    If not provided current username is used.

**\-\-enctypes ENCTYPE|ENCTYPE|..**

    Specify encryption types to be used for obtained host identity.
    Allowed (and supported on CERN Active Directory) ENCTYPEs are:

    RC4_HMAC_MD5          (RC4)
    AES128_CTS_HMAC_SHA1  (AES128)
    AES256_CTS_HMAC_SHA1  (AES256)

    Default ENCTYPEs are: RC4_HMAC_MD5|AES128_CTS_HMAC_SHA1|AES256_CTS_HMAC_SHA1

**\-\-passwordsmb**

    Saves computer account password in Samba secrets database
    (/var/lib/samba/private/secrets.tdb) for use with kerberized smbd setup.

**\-\-leavekrb5cfg**

    Store used temporary kerberos config file as:

    /tmp/cgk.krb5.conf

    This can be used by another tool in order to guarantee that same
    Active Directory Domain Controller will be used, therefore eliminating
    the need to wait for (asynchronous) AD replication.

    To use from (ba)sh shell:

    export KRB5\_CONFIG=/tmp/cgk.krb5.conf

    Warning: if /tmp/cgk.krb5.conf exists its content will be overwritten.

**\-\-testsrv**

    DEPRECATED. Please use a configuration file instead.
    Use development instance of password reset server. DO NOT USE IN PRODUCTION.

# EXAMPLES

>         cern-get-keytab
>
>         cern-get-keytab --service HTTP
>
>         cern-get-keytab --service EXTRASECURE --isolate
>
>         cern-get-keytab --alias dnsalias.cern.ch --keytab /etc/krb5.keytab.dnsalias
>
>         cern-get-keytab --user --login LOGIN --keytab ~/private/login.keytab

# ENVIRONMENT

CERN\_GET\_KEYTAB\_CONF
Full path to the configuration file. By defaults it is expected to be in /etc/cern-get-keytab.yaml.

# Configuration
The configuration file should look like this:


    server: https://lxkerbwindev.cern.ch/LxKerb.asmx
    verify_peer: false
    verify_host: false

The server field contains the full path to the endpoint.
The parameters verify\_peer and verify\_host determin if SSL verification should be performed, see CURLOPT\_SSL\_VERIFYPEER and CURLOPT\_SSL\_VERIFYHOST.
False is translated to zero, True is translated to 1 for CURLOPT\_SSL\_VERIFYPEER and 2 for CURLOPT\_SSL\_VERIFYHOST.

If no configuration file is found, production default values will be assumed.

# NOTES

All options can be abbreviated to their shortest distinctive length.

Creation and retrieval of a new keytab invalidates current one.
(except for user keytab)

# BUGS

\--alias option can be used for AD objects which are not DNS aliases

\--remove removes principal correctly from AD but leaves it in keytab
         (rerun to get it cleared from keytab)
