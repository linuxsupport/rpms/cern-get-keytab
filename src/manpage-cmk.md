% CERN-MERGE-KEYTAB(1)
% Jaroslaw Polok <jaroslaw.polok@cern.ch>, Ben Morrice <ben.morrice@cern.ch>
% October 2023

# NAME

cern-merge-keytab - utility to merge Kerberos keytabs.

# DESCRIPTION

cern-merge-keytab collects keys from input keytabs and merges these into
a single output keytab.

# SYNOPSIS

>         cern-merge-keytab   [--help]
>
>         cern-merge-keytab   --inkeytab FILENAME --outkeytab FILENAME
>                             [--append] [--cleanup]
>                             [--principal PRINCIPAL]
>                             [--enctype ENCTYPE]
>                             [--verbose] [--debug]

# OPTIONS

**\-\-help**

    Shows this help description

**\-\-inkeytab FILENAME**

    Input keytab file name. May be specified multiple times.

**\-\-outkeytab FILENAME**

    Output keytab file name.

**\-\-append**

    Append input keytab(s) keys to output keytab rather than overwriting it.

**\-\-cleanup**

    Remove all but newest (highest kvno) input keytab(s) keys before writing to output keytab.

**\-\-principal PRINCIPAL**

    Only act on input keytab(s) keys matching given Kerberos principal. May be specified multiple times.
    (principals in CERN keytabs are defined as: 'hostname.cern.ch' , 'hostname$' or 'SRVC/hostname.cern.ch')

**\-\-enctype ENCTYPE**

    Only act on input keytab(s) keys matching given encryption type(s). May be specified multiple times.
    Allowed encryption types are: ARCFOUR_HMAC,AES128_CTS_HMAC_SHA1_96,AES256_CTS_HMAC_SHA1_96

**\-\-verbose**

    Provide more information on the output.

**\-\-debug**

    Provide detailed debugging information

All options can be abbreviated to shortest distinctive length. Single minus preceding option name may be used instead of double one.

# EXIT STATUS

**0** - on success.

**1** - on error.

# EXAMPLES

>         cern-merge-keytab
>
>         cern-merge-keytab --inkeytab /etc/keytab1.kt --inkeytab /etc/keytab2.kt --outkeytab /etc/keytab3.kt
>
>         cern-merge-keytab -i /etc/kt1.kt -i /etc/kt2.kt -o /etc/kt3.kt -c -p SRVC/hostname.cern.ch -e AES256_CTS_HMAC_SHA1_96

# BUGS

??
